package DigitsToArray;

public class DigitsToArray {
    public static int[] toArray(long n) {
        String valString = String.valueOf(n);
        String reverseValue = new StringBuilder(valString).reverse().toString();
        int arrayLength = valString.length();
        int[] array = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = Integer.parseInt(reverseValue.substring(i, i + 1));
        }
        return array;
    }
}
