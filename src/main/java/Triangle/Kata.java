package Triangle;

public class Kata {
    private static final int sum = 'R' + 'G' + 'B';

    public static char triangle(final String row) {
        char[] a = row.toCharArray();
        for (int m = a.length - 1; m > 0; m--)
            for (int i = 0; i < m; i++) {
                char c1 = a[i];
                char c2 = a[i + 1];
                a[i] = c1 == c2 ? c1 : (char)(sum - c1 - c2);
            }
        return a[0];
    }
}
