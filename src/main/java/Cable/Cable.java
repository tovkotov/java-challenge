package Cable;

import java.math.BigInteger;
import java.util.Deque;
import java.util.LinkedList;

public class Cable {

    public static BigInteger calculateLength(String cable) {
        Deque<Segment> deque = new LinkedList<>();
        BigInteger totalLength = BigInteger.valueOf(0);

        for (char ch : cable.toCharArray()) {
            if (!check(ch)) {
                throw new NodeException();
            }

            if (ch == '(') {
                deque.addFirst(new Segment(ch, BigInteger.valueOf(0)));
            } else if (ch == ')') {
                if (!deque.isEmpty() && deque.peekFirst().getBracket() == '(') {
                    BigInteger tmp = deque.peekFirst().getLength().multiply(BigInteger.valueOf(2));
                    deque.removeFirst();
                    if (!deque.isEmpty())
                        deque.peekFirst().addLength(tmp);
                    else
                        totalLength = totalLength.add(tmp);
                } else if (deque.isEmpty())
                    throw new BracketException();
            } else if (!deque.isEmpty()) {
                deque.peekFirst().addLength(getElementLength(ch)); //= deque.peekFirst().length.add(getElementLength(ch));
            } else
                totalLength = totalLength.add(getElementLength(ch));
        }

        if (deque.isEmpty()) {
            return totalLength;
        } else {
            throw new BracketException();
        }

    }

    private static BigInteger getElementLength(char ch) {
        switch (ch) {
            case '-':
                return BigInteger.valueOf(1);
            case '_':
                return BigInteger.valueOf(2);
            case '=':
                return BigInteger.valueOf(3);
        }
        return BigInteger.valueOf(0);
    }

    private static boolean check(char ch) {
        return (ch == '(' || ch == ')' || ch == '_' ||
                ch == '-' || ch == '=');
    }
}

class Segment {
    private char bracket;
    private BigInteger length;

    public Segment(char bracket, BigInteger length) {
        this.bracket = bracket;
        this.length = length;
    }

    public char getBracket() {
        return bracket;
    }

    public void setBracket(char bracket) {
        this.bracket = bracket;
    }

    public BigInteger getLength() {
        return length;
    }

    public void addLength(BigInteger length) {
        this.length = this.length.add(length);
    }
}

class BracketException extends RuntimeException {
    public String toString() {
        return ("BracketException!");
    }
}

class NodeException extends RuntimeException {
    public String toString() {
        return ("NodeException!");
    }
}