package OddOrEven;

public class OddOrEven {

    public static String oddOrEven (int[] array) {
        if (array.length == 0){
            return "odd";
        }

        int sum = 0;
        for( int num : array) {
            sum = sum + num;
        }

        return sum % 2 == 0 ? "even" : "odd";
    }
}
