package CircularList;

import java.util.*;

public class CircularList<T> {
    T[] elements;
    int index = -1, length;

    public CircularList(final T... elements) {
        if (elements.length == 0) throw new NoSuchElementException("elements empty!");
        this.elements = elements;
        this.length = elements.length;
    }

    public T next() {
        if (++index == length) index = 0;
        return elements[index];
    }

    public T prev() {
        if (--index < 0) index = length - 1;
        return elements[index];
    }
}
