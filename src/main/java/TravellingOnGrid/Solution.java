package TravellingOnGrid;


public class Solution {
    public static int travelChessboard(String s) {
        long x = Integer.parseInt(s.substring(6, 7))
                - Integer.parseInt(s.substring(1, 2));
        long y = Integer.parseInt(s.substring(8, 9))
                - Integer.parseInt(s.substring(3, 4));
        return (int) (getFactorial(x + y) / (getFactorial(x) * getFactorial(y)));
    }


    public static long getFactorial(long value) {
        if (value <= 1) {
            return 1;
        }
        else {
            return value * getFactorial(value - 1);
        }
    }
}
