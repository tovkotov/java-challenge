package TrafficLights;

import junit.framework.TestCase;
import org.junit.Test;

public class TrafficLightsTest extends TestCase {
    @Test
    public void testLight() {
        assertEquals("green", TrafficLights.updateLight("red"));
        assertEquals("yellow", TrafficLights.updateLight("green"));
        assertEquals("red", TrafficLights.updateLight("yellow"));
    }
}