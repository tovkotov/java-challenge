package DigitsToArray;

import junit.framework.TestCase;

import java.util.Arrays;

public class DigitsToArrayTest extends TestCase {
    long value1 = 123_345_567;
    long value2 = 345837566;
    long valueNull;

    public void testDigit(){
        int[] exp = new int[]{7,6,5,5,4,3,3,2,1};
        assertTrue(Arrays.equals(exp, DigitsToArray.toArray(value1)));
    }

    public void testNull(){
        int[] exp = new int[]{};
        assertTrue(Arrays.equals(exp, DigitsToArray.toArray(valueNull)));
    }
}