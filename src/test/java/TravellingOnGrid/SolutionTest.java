package TravellingOnGrid;

import junit.framework.TestCase;


public class SolutionTest extends TestCase {

    public void testSomeValues1() {
        assertEquals(6  ,Solution.travelChessboard("(1 1)(3 3)"));
    }

    public void testSomeValues2() {
        assertEquals(21 ,Solution.travelChessboard("(2 3)(4 8)"));
    }

    public void testSomeValues3() {
        assertEquals(1  ,Solution.travelChessboard("(1 8)(4 8)"));
    }

    public void testSomeValues7() {
        assertEquals(330,Solution.travelChessboard("(3 1)(7 8)"));
    }



    public void testSomeValues9() {
        assertEquals(4  ,Solution.travelChessboard("(1 1)(2 4)"));
    }


}