package Triangle;

import junit.framework.TestCase;

public class KataTest extends TestCase {
    public void testRandom2() {
        String random = "RG";
        assertEquals('B', Kata.triangle(random));
    }
    public void testRandom3() {
        String random = "RGB";
        assertEquals('G', Kata.triangle(random));
    }
    public void testRandom4() {
        String random = "RGBB";
        assertEquals('G', Kata.triangle(random));
    }
    public void testRandom7() {
        String random = "RGBGGGR";
        assertEquals('B', Kata.triangle(random));
    }

    public void testRandom100500() {
        String random = "RBRGBRBGGRRRBGBBBGG";
        assertEquals('G', Kata.triangle(random));
    }
}