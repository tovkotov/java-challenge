package OddOrEven;


import junit.framework.TestCase;

public class OddOrEvenTest extends TestCase {
    String expOdd = "odd"; // нечетное
    String expEven = "even"; // четное

    public void testNull  () {
        int[] array = new int[] {};
        assertEquals(expOdd, OddOrEven.oddOrEven(array));
    }

    public void testEven  () {
        int[] array = new int[] {1,1,2,2,4,4,8,8,0,0};
        assertEquals(expEven, OddOrEven.oddOrEven(array));
    }

    public void testOdd  () {
        int[] array = new int[] {1,1,1,3,3,100};
        assertEquals(expOdd, OddOrEven.oddOrEven(array));
    }


}