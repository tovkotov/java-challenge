package Cable;

import junit.framework.TestCase;

import java.math.BigInteger;

public class CableTest extends TestCase {


//        public void testSimple() {
//            assertEquals(BigInteger.valueOf(4), Cable.calculateLength("----"));
//            assertEquals(BigInteger.valueOf(6), Cable.calculateLength("-__-"));
//            assertEquals(BigInteger.valueOf(9), Cable.calculateLength("-_=_-"));
//            assertEquals(BigInteger.valueOf(4), Cable.calculateLength("(--)"));
//            assertEquals(BigInteger.valueOf(6), Cable.calculateLength("(-_)"));
//            assertEquals(BigInteger.valueOf(8), Cable.calculateLength("_(-_)"));
//        }

    public void testSimple0() {
        assertEquals(BigInteger.valueOf(2), Cable.calculateLength("--"));
    }
    public void testSimple1() {
        assertEquals(BigInteger.valueOf(4), Cable.calculateLength("((-))"));
    }
    public void testSimple2() {
        assertEquals(BigInteger.valueOf(28), Cable.calculateLength("(--)---===---==="));
    }
    public void testSimple3() {
        assertEquals(BigInteger.valueOf(4), Cable.calculateLength("(-()-)"));
    }
    public void testSimple4() {
        assertEquals(BigInteger.valueOf(8), Cable.calculateLength("((--))"));
    }
    public void testSimple5() {
        assertEquals(BigInteger.valueOf(10), Cable.calculateLength("-((-()-))-"));
    }
    public void testSimple6() {
        assertEquals(BigInteger.valueOf(55), Cable.calculateLength("-((-=((-)-)_)-)-="));
    }

    public void testSimple7() {
        assertEquals(BigInteger.valueOf(4), Cable.calculateLength("-((-=((-!)-)_)-)-="));
    }
    public void testSimple8() {
        assertEquals(BigInteger.valueOf(4), Cable.calculateLength("-((-=((-()-)_)-)-="));
    }
    public void testSimple9() {
        assertEquals(BigInteger.valueOf(4), Cable.calculateLength(")"));
    }

        public void testHarder(){
            assertEquals(BigInteger.valueOf(15), Cable.calculateLength("_(-(_))="));
            assertEquals(BigInteger.valueOf(33), Cable.calculateLength("_((=)((-))(_))="));
        }

    }
